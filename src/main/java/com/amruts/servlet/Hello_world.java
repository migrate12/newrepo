package com.amruts.servlet;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



public class Hello_world extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(Hello_world.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().write("<html><body>Hello World</body></html>");
        IssueService issueservice=ComponentAccessor.getIssueService();
        IssueInputParameters issueinputparameters = issueservice.newIssueInputParameters();
//        Project p = ComponentAccessor.getProjectManager().getProjectObjByKey("TES");--> 
//        Long id= p.getId();
//        resp.getWriter().write("project id is:"+id);
        issueinputparameters.setProjectId((long) 10201);
        issueinputparameters.setIssueTypeId("2");
        issueinputparameters.setSummary("Task");
        issueinputparameters.setAssigneeId("admin");
        issueinputparameters.setReporterId("admin");
        //issueinputparameters.setStatusId("2");
        //issueinputparameters.setPriorityId("2");
        //issueinputparameters.setResolutionId("2");
        issueinputparameters.setDescription("new issue");
        //issueinputparameters.setComment("comment");
        //issueinputparameters.setSecurityLevelId(10000L);
        //issueinputparameters.setFixVersionIds(10000L, 10001L);
        System.out.println("Hello World! test_servlet");
        
       ApplicationUser appuser=  ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        IssueService.CreateValidationResult result = issueservice.validateCreate(appuser, issueinputparameters);
        if(result.getErrorCollection().hasAnyErrors())
         {
       
        	System.out.println(result.getErrorCollection().hasAnyErrors());
        
          }
        else{
        	issueservice.create(appuser,result);
        	
        }

}
}